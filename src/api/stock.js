import { getJson } from '../helpers/http';

const API_ROOT = 'https://api.iextrading.com/1.0';

/**
 * Get list of stocks in focus
 *
 * @return {Array} An array of stocks if successful, else an empty array
 */
function getList() {
  return getJson(`${API_ROOT}/stock/market/list/infocus`).catch(error => {
    // TODO: log error?
    return [];
  });
}

/**
 * Get companies
 *
 * @param {Array} symbols An array of stock symbols corresponding to companies to get
 * @return {Array} An array of companies if successful, else an empty array
 */
function getCompanies(symbols) {
  return getJson(
    `${API_ROOT}/stock/market/batch?symbols=${symbols.join()}&types=company`
  )
    .then(json => Object.keys(json).map(key => json[key].company))
    .catch(error => {
      // TODO: log error?
      return [];
    });
}

/**
 * Get stocks
 * Includes quote and company data
 *
 * @param {Array} symbols An array of stock symbols to get data on
 * @return {Object} An object indexed by `symbols`. Each symbol has a `quote` and `company` prop
 */
function getStocks(symbols) {
  return getJson(
    `${API_ROOT}/stock/market/batch?symbols=${symbols.join()}&types=quote,company`
  ).catch(error => {
    // TODO: log error?
    return {};
  });
}

export default {
  getList,
  getCompanies,
  getStocks
};
