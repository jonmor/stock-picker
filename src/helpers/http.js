/**
 * Check status
 *
 * @param {Response} res The response to check
 * @return {Promise} If response is successful, the promise will resolve to a response object. Otherwise it'll reject with the HTTP status text
 */
function checkStatus(res) {
  return res.ok ? Promise.resolve(res) : Promise.reject(Error(res.statusText));
}

/**
 * Create a GET request
 *
 * @param {string} url
 * @return {Promise} @see checkStatus
 */
export function get(url) {
  return fetch(url).then(checkStatus);
}

/**
 * Create a GET request and extract the JSON body
 *
 * @param {string} url
 * @return {Promise} If response is successful, the promise will resolve to a JSON object. Otherwise it'll reject with the HTTP status text
 */
export function getJson(url) {
  return get(url).then(res => res.json());
}
