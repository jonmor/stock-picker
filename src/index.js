import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import * as serviceWorker from './serviceWorker';

// Base CSS
import 'normalize.css/normalize.css';
import './css/bootstrap/_reboot.css';
import './css/_base.css';

// Atomic CSS
import './css/tachyons/tachyons.css';
import './css/tachyons/_border-colors.css';
import './css/tachyons/_border-colors-pseudo.css';
import './css/tachyons/_border-colors-hs.css';
import './css/tachyons/_letter-spacing.css';
import './css/tachyons/_box-shadow-e.css';
import './css/tachyons/_skins.css';
import './css/tachyons/_skins_pseudo.css';
import './css/tachyons/_transitions.css';

ReactDOM.render(<App />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
