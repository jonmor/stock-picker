import React, { Component } from 'react';
import PropTypes from 'prop-types';

import SearchBar from './SearchBar';
import AutoComplete, {
  findNewActiveOnMove,
  DIRECTION_DOWN,
  DIRECTION_UP
} from './AutoComplete';

import styles from '../../css/modules/SearchBar.module.css';

class SearchBarWithAutoComplete extends Component {
  constructor(props) {
    super(props);

    this.component = React.createRef();

    this.handleOutsideClick = this.handleOutsideClick.bind(this);
    this.handleFocus = this.handleFocus.bind(this);
    this.handleBlur = this.handleBlur.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleKeyDown = this.handleKeyDown.bind(this);
    this.handleButtonClick = this.handleButtonClick.bind(this);
    this.handleOutsideClick = this.handleOutsideClick.bind(this);

    this.filterAutoCompleteOptions = this.filterAutoCompleteOptions.bind(this);
    this.handleAutoCompleteOptionClick = this.handleAutoCompleteOptionClick.bind(
      this
    );
    this.handleAutoCompleteOptionHover = this.handleAutoCompleteOptionHover.bind(
      this
    );
    this.handleAutoCompleteMouseLeave = this.handleAutoCompleteMouseLeave.bind(
      this
    );

    this.state = {
      value: '',
      showAutoComplete: false,
      activeAutoComplete: null,
      isInputFocused: false
    };
  }

  componentDidMount() {
    document.addEventListener('click', this.handleOutsideClick);
  }

  componentWillUnmount() {
    document.removeEventListener('click', this.handleOutsideClick);
  }

  handleFocus() {
    const showAutoComplete = this.state.value.length;
    this.setState({ showAutoComplete, isInputFocused: true });
  }

  handleBlur() {
    this.setState({ isInputFocused: false });
  }

  handleChange(e) {
    const { value } = e.target;
    const showAutoComplete = value.length ? true : false;

    this.setState({ value, showAutoComplete });
  }

  handleKeyDown(e) {
    const { key } = e;
    const { activeAutoComplete } = this.state;

    switch (key) {
      case 'Escape':
        this.setState({ showAutoComplete: false, activeAutoComplete: null });
        break;
      case 'Tab':
        this.setState({ showAutoComplete: false, activeAutoComplete: null });
        break;
      case 'Enter':
        const value =
          activeAutoComplete !== null ? activeAutoComplete : this.state.value;

        this.setState({
          value,
          showAutoComplete: false,
          activeAutoComplete: null
        });

        this.props.onSearch(value);
        break;
      case 'ArrowDown':
      case 'ArrowUp':
        const direction = key === 'ArrowDown' ? DIRECTION_DOWN : DIRECTION_UP;
        const newActive = findNewActiveOnMove({
          options: this.filterAutoCompleteOptions(),
          direction,
          currActive: activeAutoComplete,
          maxOptions: this.props.maxAutoCompleteOptions
        });

        this.setState({ activeAutoComplete: newActive });
        break;
      default:
      // do nothing
    }
  }

  handleButtonClick() {
    this.setState({ showAutoComplete: false, activeAutoComplete: null });
    this.props.onSearch(this.state.value);
  }

  handleOutsideClick(e) {
    if (!this.component.current.contains(e.target))
      this.setState({ showAutoComplete: false, activeAutoComplete: null });
  }

  // TODO: better way to build computed state?
  filterAutoCompleteOptions() {
    const { value } = this.state;

    const filtered = this.props.autoCompleteOptions.filter(option =>
      option.includes(value.toUpperCase())
    );

    const regex = new RegExp(value.toUpperCase());
    return filtered.sort((a, b) => {
      const firstMatchIndex = a.match(regex).index - b.match(regex).index;

      // Sort the options if the match indices are different
      if (firstMatchIndex !== 0) return firstMatchIndex;

      // Else sort by string length
      return a.length - b.length;
    });
  }

  handleAutoCompleteOptionClick(option) {
    this.setState({ value: option, showAutoComplete: false });
    this.props.onSearch(option);
  }

  handleAutoCompleteOptionHover(option) {
    this.setState({ activeAutoComplete: option });
  }

  handleAutoCompleteMouseLeave() {
    this.setState({ activeAutoComplete: null });
  }

  render() {
    const { showAutoComplete, isInputFocused } = this.state;
    const filteredAutoCompleteOptions = this.filterAutoCompleteOptions();

    const classNames = {
      component: `${styles.hover} relative mw5`,
      focus: `${
        styles.hoverTarget
      } absolute top-0 w-100 ba b--ced4da br2 transition-focus`,
      ac: {
        skin: {
          input: 'w-100 pv1 pl2 pr4 ba b--transparent bg-transparent'
        },
        container: 'overflow-hidden bg-fff br2 br--bottom bt b--80bdff'
      }
    };

    if (isInputFocused || showAutoComplete)
      classNames.focus += ' -b--80bdff shadow-focus';

    if (!filteredAutoCompleteOptions.length || !showAutoComplete)
      classNames.ac.container += ' dn';

    return (
      <div ref={this.component} className={classNames.component}>
        {/* The z-index pops the search bar above the auto complete list */}
        <div className="relative z-1">
          <SearchBar
            value={this.state.value}
            maxLength={this.props.maxLength}
            onFocus={this.handleFocus}
            onBlur={this.handleBlur}
            onChange={this.handleChange}
            onKeyDown={this.handleKeyDown}
            onButtonClick={this.handleButtonClick}
            placeholder="Enter stock symbol"
            skin={classNames.ac.skin}
          />
        </div>

        <div className={classNames.focus}>
          {/* The <div> below with padding ensures the auto complete list doesn't hide under the search bar */}
          <div className="pt4" />
          <div className={classNames.ac.container}>
            <AutoComplete
              options={filteredAutoCompleteOptions}
              active={this.state.activeAutoComplete}
              maxOptions={this.props.maxAutoCompleteOptions}
              onOptionClick={this.handleAutoCompleteOptionClick}
              onOptionHover={this.handleAutoCompleteOptionHover}
              onMouseLeave={this.handleAutoCompleteMouseLeave}
            />
          </div>
        </div>
      </div>
    );
  }
}

SearchBarWithAutoComplete.defaultProps = {
  maxAutoCompleteOptions: 5
};

SearchBarWithAutoComplete.propTypes = {
  onSearch: PropTypes.func,
  maxAutoCompleteOptions: PropTypes.number,
  autoCompleteOptions: PropTypes.array,
  maxLength: PropTypes.number
};

export default SearchBarWithAutoComplete;
