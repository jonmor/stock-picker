import React from 'react';
import PropTypes from 'prop-types';

import { ReactComponent as Icon } from '../../imgs/search-solid.svg';

function SearchBar({ onButtonClick, value, skin, ...inputProps }) {
  const defautSkin = {
    input:
      'w-100 pv1 pl2 pr4 transition-focus br2 ba b--ced4da b-focus--80bdff shadow-focus-focus',
    button:
      'pointer absolute right-0 top-0 bottom-0 w2 pa0 pt1 br2 br--right bg-animate bg-transparent bg-hover--eee ba b--transparent b-hover--ced4da transition-focus b-focus--80bdff shadow-focus-focus'
  };

  const classNames = { ...defautSkin, ...skin };

  return (
    <div className="relative">
      <input
        type="text"
        value={value}
        {...inputProps}
        className={classNames.input}
      />
      <button onClick={onButtonClick} className={classNames.button}>
        <Icon className="w1" />
      </button>
    </div>
  );
}

SearchBar.propTypes = {
  onButtonClick: PropTypes.func,
  value: PropTypes.string,
  skin: PropTypes.object
};

export default SearchBar;
