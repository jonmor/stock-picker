import React from 'react';
import PropTypes from 'prop-types';
import merge from 'lodash.merge';

export const DIRECTION_DOWN = 'down';
export const DIRECTION_UP = 'up';
const MAX_OPTIONS_DEFAULT = 5;

/**
 * Find the new active option when moving down/up an `options` list
 *
 * This function takes a single args object with the following keys:
 * @param {array} options The list of options
 * @param {string} direction Either `DIRECTION_DOWN` or `DIRECTION_UP`
 * @param {string} currActive The current active option
 * @param {integer} maxOptions The max number of visible options
 * @return {(string|undefined)} The new active option or undefined
 */
export function findNewActiveOnMove({
  options,
  direction = DIRECTION_DOWN,
  currActive = null,
  maxOptions = MAX_OPTIONS_DEFAULT
}) {
  // Guard against no options
  if (options.length === 0) return;

  // Handle single option
  if (options.length === 1) {
    return options[0];
  }

  const lastIndex =
    options.length > maxOptions ? maxOptions - 1 : options.length - 1;
  const isDown = direction === DIRECTION_DOWN;
  let activeIndex = null;

  // Handle no currently active option
  if (currActive === null) {
    activeIndex = isDown ? 0 : lastIndex;
    return options[activeIndex];
  }

  // Handle currently active option
  activeIndex = options.findIndex(option => option === currActive);
  const step = isDown ? 1 : -1;
  activeIndex += step;

  // Handle out-of-bound indices
  if (activeIndex < 0) activeIndex = lastIndex;
  else if (activeIndex > lastIndex) activeIndex = 0;

  return options[activeIndex];
}

function Option({ option, isActive, classNames, onClick, onMouseEnter }) {
  const { base, active } = classNames;
  const className = isActive ? `${base} ${active}` : base;

  return (
    <li
      className={className}
      onClick={e => onClick(option)}
      onMouseEnter={e => onMouseEnter(option)}
    >
      {option}
    </li>
  );
}

function AutoComplete({
  skin,
  options,
  active,
  maxOptions = MAX_OPTIONS_DEFAULT,
  onOptionClick,
  onOptionHover,
  onMouseLeave
}) {
  const defaultSkin = {
    component: 'ma0 w-100 pl0 list',
    option: {
      active: 'bg-eee',
      base: 'pointer bg-animate pv1 ph2 bt b--eee'
    }
  };
  const classNames = merge({}, defaultSkin, skin);

  const limitedOptions = options.slice(0, maxOptions);

  return (
    <ul className={classNames.component} onMouseLeave={onMouseLeave}>
      {limitedOptions.map(option => (
        <Option
          option={option}
          key={option}
          isActive={active === option}
          classNames={classNames.option}
          onClick={onOptionClick}
          onMouseEnter={onOptionHover}
        />
      ))}
    </ul>
  );
}

AutoComplete.propTypes = {
  skin: PropTypes.object,
  options: PropTypes.array,
  active: PropTypes.string,
  maxOptions: PropTypes.number,
  onOptionClick: PropTypes.func,
  onOptionHover: PropTypes.func,
  onMouseLeave: PropTypes.func
};

export default AutoComplete;
