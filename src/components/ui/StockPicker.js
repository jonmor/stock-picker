import React, { Component } from 'react';

import Section from '../layouts/Section';
import SearchBarWithAutoComplete from './SearchBarWithAutoComplete';

import stockApi from '../../api/stock';

function Details({ symbol, latestPrice, description, classNames }) {
  const { textHeading } = classNames;

  return (
    <div>
      <h3 className={`${textHeading} mt4`}>Symbol</h3>
      <p>{symbol}</p>

      <h3 className={textHeading}>Current Stock Price</h3>
      <p>${latestPrice}</p>

      <h3 className={textHeading}>Description</h3>
      <p className="mb0">{description}</p>
    </div>
  );
}

class StockPicker extends Component {
  constructor(props) {
    super(props);

    this.handleSearchValue = this.handleSearchValue.bind(this);
    this.symbolExists = this.symbolExists.bind(this);
    this.hasSearchedSymbol = this.hasSearchedSymbol.bind(this);

    this.state = {
      quotes: {},
      companies: {},
      symbol: '',
      searchValue: ''
    };
  }

  componentDidMount() {
    // TODO: move data retrieval to `App.js` component
    // Set initial state
    stockApi
      .getList()
      .then(quotes => {
        // First time getting `quotes` data
        if (quotes.length === 0) return;

        // Index by symbol for quick access
        const quotesBySymbol = quotes.reduce((obj, quote) => {
          obj[quote.symbol] = quote;
          return obj;
        }, {});

        this.setState({ quotes: quotesBySymbol });

        // Get corresponding company data
        return stockApi.getCompanies(Object.keys(quotesBySymbol));
      })
      .then(companies => {
        // First time getting `companies` data
        if (companies.length === 0) return;

        // Index by symbol for quick access
        const companiesBySymbol = companies.reduce((obj, company) => {
          obj[company.symbol] = company;
          return obj;
        }, {});

        this.setState({ companies: companiesBySymbol });
      });
  }

  symbolExists() {
    const { quotes, symbol } = this.state;
    return Boolean(quotes[symbol]);
  }

  hasSearchedSymbol() {
    const { quotes, symbol } = this.state;
    return quotes[symbol] !== undefined;
  }

  handleSearchValue(value) {
    this.setState({ searchValue: value });

    // Guard against empty strings
    if (!value.trim().length) return;

    // Match case to stock API
    const upperCased = value.trim().toUpperCase();

    // Get stock data if not cached
    // NOTE: a `null` value means a request for `symbol` data had already been made (and it returned an empty response)
    if (this.state.quotes[upperCased] === undefined) {
      stockApi.getStocks([upperCased]).then(symbols => {
        const symbolData = symbols[upperCased];
        const quote = symbolData ? symbolData.quote : null;
        const company = symbolData ? symbolData.company : null;

        const quotes = { ...this.state.quotes, [upperCased]: quote };
        const companies = { ...this.state.companies, [upperCased]: company };
        this.setState({ quotes, companies, symbol: upperCased });
      });
    } else {
      // Stock data is cached
      this.setState({ symbol: upperCased });
    }
  }

  render() {
    const { quotes, companies, symbol, searchValue } = this.state;
    const latestPrice = quotes[symbol] ? quotes[symbol].latestPrice : null;

    // NOTE: it is possible a stock only has quote data, and not company data
    const description = companies[symbol]
      ? companies[symbol].description
      : 'Not available.';

    // Filter out quotes without data
    const autoCompleteOptions = Object.keys(quotes).filter(
      quote => quotes[quote]
    );

    return (
      <div>
        <Section>
          <SearchBarWithAutoComplete
            autoCompleteOptions={autoCompleteOptions}
            onSearch={this.handleSearchValue}
            maxLength={5}
          />

          {this.hasSearchedSymbol() && !this.symbolExists() && (
            <p className="mb0">No results found for "{searchValue}"</p>
          )}
        </Section>

        <div className="bt bw2 mt4 c-19a974" />

        {this.symbolExists() && (
          <Section>
            <div className="pb4">
              <Details
                symbol={symbol}
                latestPrice={latestPrice}
                description={description}
                classNames={{ textHeading: 'f5 fw6' }}
              />
            </div>
          </Section>
        )}
      </div>
    );
  }
}

export default StockPicker;
