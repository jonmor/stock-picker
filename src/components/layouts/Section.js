import React from 'react';

function Section({ children }) {
  return <div className="ph3 ph4-ns">{children}</div>;
}

export default Section;
