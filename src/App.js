import React, { Component } from 'react';

import Section from './components/layouts/Section';
import StockPicker from './components/ui/StockPicker';

class App extends Component {
  render() {
    return (
      <div className="center mt3 mw6 shadow-4 bg-fff bb bw2 b--001b44">
        <Section>
          <h1 className="mt0 pt4 f4 ttu tracked-0_1">Stock Picker</h1>
        </Section>
        <StockPicker />
      </div>
    );
  }
}

export default App;
